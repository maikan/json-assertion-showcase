package dto.common

data class Account(
    val number: String,
    val type: String,
    val description: String,
    val amount: Amount
)