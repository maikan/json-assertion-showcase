package dto.common

data class Amount(
    val value: Long,
    val currency: String,
    val minorUnits: Int
)