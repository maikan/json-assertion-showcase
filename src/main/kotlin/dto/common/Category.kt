package dto.common

data class Category(
    val id: String,
    val name: String,
    val color: String,
    val icon: String
)