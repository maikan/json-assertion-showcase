package dto.common

data class DynamicField(
    val type: String,
    val label: String,
    val value: String
)