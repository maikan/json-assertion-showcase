package dto.history

import dto.common.Amount
import dto.common.Category
import dto.common.DynamicField

data class OperationHistoryCardResponse(
    val id: String,
    val dateTime: String,
    val title: String,
    val logoUrl: String? = null,
    val direction: String,
    val status: String? = null,
    val amount: Amount,
    val category: Category? = null,
    val fields: List<DynamicField>
) 