package dto.multistep

import com.google.gson.JsonElement
import dto.common.Account
import lombok.EqualsAndHashCode

@EqualsAndHashCode(exclude = ["transactionId"])
data class TabModel(
    val key: String,
    val transactionId: String,
    val fields: List<ResponseFieldModel>,
    val step: StepModel,
    val headerInfo: HeaderInfoModel
)

data class HeaderInfoModel(
    val title: String,
    val subtitle: String,
    val group: String,
    val groupDescription: String,
    val imageURL: String,
    val analyticsLabel: String
)

data class StepModel(
    val number: Int,
    val type: String
)

@EqualsAndHashCode(exclude = ["value", "accounts"])
data class ResponseFieldModel(
    val subtitle: String,
    val id: String,
    val label: String,
    val type: String,
    val accounts: List<Account>,
    val required: Boolean,
    val value: JsonElement
)

data class TabList(
    val tabs: List<TabModel>
)