package dto.providers

data class Content(
    val id: String,
    val companyName: String? = null,
    val inn: String,
    val formType: String,
    val providerGroup: String,
    val providerGroupDescription: String,
    val imageURL: String,
    val fullName: String,
    val shortName: String,
    val route: String,
    val type: String,
    val system: String,
    val providerId: String,
    val code: String,
    val operationGroup: String
)

data class Metadata(
    val totalElements: Int,
    val totalPages: Int,
    val pageSize: Int,
    val pageNumber: Int,
    val last: Boolean,
    val description: String
)

data class Operator(
    val name: String,
    val code: String
)

data class Provider(
    val content: List<Content>,
    val metadata: Metadata
)

data class Providers(
    val providers: List<Content>
)

