package singleton

import com.google.gson.Gson

object GsonInstance {
    
    val gsonInstance = Gson()
}