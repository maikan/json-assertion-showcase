package utils

import singleton.GsonInstance

object ExpectedResults {

    val PATH_TO_EXPECTED_FOLDER = "expectedjson/"

    fun <T> getObjectFromJson(fileName: String, clazz: Class<T>): T = GsonInstance.gsonInstance.fromJson(
        FileUtils.readContent(PATH_TO_EXPECTED_FOLDER + fileName), clazz
    )
}