package utils

import lombok.SneakyThrows
import org.springframework.core.io.ClassPathResource
import java.nio.file.Files

class FileUtils {
    
    companion object FileUtils {
        
        @SneakyThrows
        fun readContent(filePath: String): String {
            val file = ClassPathResource(filePath).file
            return String(Files.readAllBytes(file.toPath()))
        }
    }
}