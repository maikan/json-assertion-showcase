package utils

enum class JsonName {
    EXP0,
    EXP1,
    EXP2,
    EXP3,
    EXP4,
    EXP5,
    EXP6,
    EXP7,
    EXP8,
    EXP9;
    
    fun getFileName(): String = "${toString().toLowerCase()}.json"
}