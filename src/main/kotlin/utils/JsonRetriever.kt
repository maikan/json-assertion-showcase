package utils

import dto.history.OperationHistoryCardResponse
import dto.multistep.TabList
import dto.providers.Content

interface JsonRetriever {
    
    val fileNames: List<JsonName>

    companion object Errors {

        fun generateMappingError(jsonName: JsonName): Nothing =
            throw InternalError("В списке нет объекта для файла ${jsonName.getFileName()}")
    }
}

class MultistepRetriever(override val fileNames: List<JsonName>) : JsonRetriever {
    
    val jsons: Map<JsonName, TabList> =
        fileNames.associateWith { ExpectedResults.getObjectFromJson(it.getFileName(), TabList::class.java) }

    operator fun get(jsonName: JsonName): TabList = jsons[jsonName] ?: JsonRetriever.generateMappingError(jsonName)
}

class HistoryRetriever(override val fileNames: List<JsonName>) : JsonRetriever {
    
    val jsons: Map<JsonName, OperationHistoryCardResponse> =
        fileNames.associateWith {
            ExpectedResults.getObjectFromJson(
                it.getFileName(),
                OperationHistoryCardResponse::class.java
            )
        }

    operator fun get(jsonName: JsonName): OperationHistoryCardResponse =
        jsons[jsonName] ?: JsonRetriever.generateMappingError(jsonName)
}

class ProviderRetriever(override val fileNames: List<JsonName>) : JsonRetriever {
    
    val jsons: Map<JsonName, Content> =
        fileNames.associateWith { ExpectedResults.getObjectFromJson(it.getFileName(), Content::class.java) }

    operator fun get(jsonName: JsonName): Content =
        jsons[jsonName] ?: JsonRetriever.generateMappingError(jsonName)
}