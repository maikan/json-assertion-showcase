package no.json

import dto.multistep.TabList
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import utils.JsonName.*
import utils.MultistepRetriever

@Tag("NO-JSON")
class TestClassOne {
    
    var multistepRetriever = MultistepRetriever(listOf(EXP0, EXP1, EXP2, EXP3))
    
    @Test
    @DisplayName("exp0 no json")
    fun exp0NoJson() {
        val actual: TabList = multistepRetriever[EXP0]
        
        assertAll({assertEquals("Оплатить", actual.tabs[0].key)})
    }
}